package hm5;

import java.util.ArrayList;
import java.util.Arrays;

public class Family {
    private Human2 father, mother;
    private ArrayList<Human2> children = new ArrayList<>();
    private Pet2 pet;
    private int count_of_members = 2;

    public Family(Human2 father, Human2 mother, Pet2 pet) {
        this.father = father;
        this.mother = mother;
        this.pet = pet;

    }

    public Human2 getFather() {
        return father;
    }

    public Human2 getMother() {
        return mother;
    }

    private String[] children_names() {
        String[] s = new String[getCount_of_members() - 2];
        for (int i = 0; i < count_of_members - 2; i++) {
            s[i] = children.get(i).getName();
        }
        return s;
    }


    public Pet2 getPet() {
        return pet;
    }

    public void setFather(Human2 father) {
        this.father = father;
    }

    public void setMother(Human2 mother) {
        this.mother = mother;
    }


    public void setPet(Pet2 pet) {
        this.pet = pet;
    }

    public void addChild(Human2 child) {
        children.add(child);
        count_of_members++;
    }

    public boolean deleteChild(int index) {
        if (children.size() == 0 || index > children.size()) return false;

        children.remove(index);


        count_of_members--;
        return true;

    }


    public int getCount_of_members() {
        System.out.println(count_of_members);
        return count_of_members;
    }

    @Override
    public String toString() {
        return String.format("The %ss' family{father:%s, mother:%s, children:%s , pet:%s }", father.getSurname(), father.getName(), mother.getName(), Arrays.toString(children_names()), pet.getSpecie());
    }


}
