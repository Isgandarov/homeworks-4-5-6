package hm5;

public class App2 {
    public static void main(String[] args) {
        final int max_limitation = 5;
        Human2 child[] = new Human2[max_limitation];
        Human2 mother = new Human2("Anna", "Morty", 110, 1982, new String[][]{{"Monday", "swimming"}, {"Saturday", "cooking"}});
        Human2 father = new Human2("John", "Morty", 110, 1981, new String[][]{{"Tuesday", "eating"}, {"Thursday", "working"}});
        Pet2 pet = new Pet2("dog", "Maks", new String[]{"eat", "bark"}, 6, 28);
        Family family = new Family(father, mother, pet);
        child[0] = new Human2("Tom", "Morty", 110, 2001, new String[][]{{"Wednesday", "sleeping"}, {"Monday", "reading"}});
        child[1] = new Human2("Bob", "Morty", 110, 2001, new String[][]{{"Monday", "singing"}, {"Friday", "hangout with friends"}});
        child[2] = new Human2("Charlie", "A", 110, 2001, new String[][]{{"Sunday", "eating"}, {"Wednesday", "watching TV"}});

        for (Human2 obj : child) {
            if (father.equals(obj))
                family.addChild(obj);
        }

        System.out.println(family.toString());


        family.deleteChild(1);

        System.out.println(family);


    }
}
