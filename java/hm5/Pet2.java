package hm5;

import java.util.Arrays;

public class Pet2 {
    private String specie, nickname;
    private String[] habits;
    private int age, trickLevel;

    public Pet2(String specie, String nickname, String[] habits, int age, int trickLevel) {
        this.specie = specie;
        this.nickname = nickname;
        this.habits = habits;
        this.age = age;
        this.trickLevel = trickLevel;
    }

    public int getAge() {
        return age;
    }

    public String getSpecie() {
        return specie;
    }

    public int getTrickLevel() {
        return trickLevel;
    }



    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}", specie, nickname, age, trickLevel, Arrays.toString(habits));

    }


}
